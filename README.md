# NOTA Labs Project #

This project repository contains NOTA homeworks from Human-Like Artificial Agents course of Charles University - Prague.

## Author: Federico Sergio

### Credits ###

Special thanks to [Petr Houška](https://github.com/petrroll) who shared his work and inspired me.

Icons used in the project are downloaded from www.flaticon.com. 
For more information see the [Credit Policy](https://support.flaticon.com/hc/en-us/articles/207248209-How-I-must-insert-the-attribution-)

- [Wind Icon](https://www.flaticon.com/free-icon/wind_2676047) made by [Good Ware](https://www.flaticon.com/authors/good-ware) from www.flaticon.com
- [Load Icon](https://www.flaticon.com/free-icon/loading_1873214) made by [Freepik](https://www.freepik.com/) from www.flaticon.com
- [Unload Icon](https://www.flaticon.com/free-icon/unloading_1873232) made by [Freepik](https://www.freepik.com/) from www.flaticon.com
- [GameOver Icon](https://www.flaticon.com/free-icon/game-over_2927801?term=game%20over&page=1&position=9) made by [Good Ware](https://www.flaticon.com/authors/good-ware) from www.flaticon.com
- [DrawMarks Icon](https://www.flaticon.com/free-icon/pencil_2919745) made by [Freepik](https://www.freepik.com/) from www.flaticon.com
- [followPath Icon](https://www.flaticon.com/free-icon/purpose_992638?term=path&page=1&position=24) made by [geotatah](https://www.flaticon.com/authors/geotatah) from www.flaticon.com
- [ctp2 Icon](https://www.flaticon.com/free-icon/mission_3064864) made by [Pixel Perfect](https://icon54.com/) from www.flaticon.com
- [ttdr Icon](https://www.flaticon.com/free-icon/helicopter_2400181) made by [surang](https://www.flaticon.com/authors/surang) from www.flaticon.com