local sensorInfo = {
	name = "FindHighPoints",
	desc = "Gets all the map locations having a high point",
	author = "Federico Sergio",
	date = "2020-06-27",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

local SpringGetGroundHeight = Spring.GetGroundHeight 

-- @description return all points that could be considered hills (their height > 2/3 lowest<>highest)
return function(tresholdAltitude, partioningDensity)
	tresholdAltitude = tresholdAltitude or 2/3
	partioningDensity = partioningDensity or 100

	local mapLength = Game.mapSizeX
 	local mapWidth = Game.mapSizeZ
	
	local lowestAltitude, highestAltitude = Spring.GetGroundExtremes()
	local heightTresholdForHill = lowestAltitude + (highestAltitude - lowestAltitude) * tresholdAltitude

	local hills = {}
	local hillId = 1
	
	local unitID = units[1]
	for x=0, mapLength, mapLength / 100 do
		for y=0, mapWidth, mapWidth / 100 do
			local height = Spring.GetGroundOrigHeight(x, y)
			
			if height > heightTresholdForHill then
				hills[hillId] = Vec3(x, height, y)
				hillId = hillId + 1
			end
		end
	end

	return hills

end