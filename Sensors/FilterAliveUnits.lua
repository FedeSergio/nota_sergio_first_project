local sensorInfo = {
	name = "FilterAliveUnits",
	desc = "Filters alive units.",
	author = "Federic Sergio",
	date = "2020-07-30",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

SpringGetUnitHealth = Spring.GetUnitHealth

-- @description filters dead units
return function(uids)

	local aliveUIds = {}
	local j = 1

	for i=1, #uids do

		local uid = uids[i]
		local currHealth = SpringGetUnitHealth(uid)

		if currHealth ~= nil and currHealth > 0 then
			aliveUIds[j] = uid
			j = j + 1
		end
	end

	return aliveUIds

end