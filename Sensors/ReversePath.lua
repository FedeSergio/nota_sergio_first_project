local sensorInfo = {
	name = "ReversePath",
	desc = "Reverses order of values in path'.",
	author = "Federico Sergio",
	date = "2020-07-30",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

local SpringGetGroundHeight = Spring.GetGroundHeight 

-- @description reverses order of values in map
return function(pathSteps)

	local reversedPath = {}
	
	local len = #pathSteps
	for i = 1, len do
		reversedPath[i] = pathSteps[len - i + 1]
	end

	return reversedPath

end