local sensorInfo = {
	name = "CreateGroups",
	desc = "Create random groups from unit array",
	author = "Federico Sergio",
	date = "2020-06-27",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- @description create groups from unit array
return function(numerOfGroups, unitsToGroup)
    unitsToGroup = unitsToGroup or units

    local groups = {}
    for x=1, unitsToGroup.length do
        local groupIndex = (x % numerOfGroups) + 1
        if groups[groupIndex] == nil then groups[groupIndex] = {} end
        groups[groupIndex][#groups[groupIndex] + 1] = unitsToGroup[x]
    end

	return groups

end