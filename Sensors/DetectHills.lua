local sensorInfo = {
	name = "DetectHills",
	desc = "Select always only one position out of bunch of near high points.",
	author = "Federico Sergio",
	date = "2020-06-30",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- @description from each close area return exactly one location / {location, [location keys]}
-- fast implementation of one pass of k-means that expects no overlap between areas
return function(locations, hillsArea)
	hillsArea = hillsArea or 512

	closeLocs = {}
	closeLocId = 1 --number of locations seen in specific areas, needed for average position calculation

	for key, loc in pairs(locations) do

		local isCloseToAlreadySelected = false
		for selectedAreaKey, selectedArea in pairs(closeLocs) do
			-- current location is within an already seen area
			if loc:Distance(selectedArea[1]) < hillsArea then
				
				-- newareaCentre = (oldCentre * numberOfLocationsInNeighbSofar + newLocation) / (numberOfLocationsInNeighbSofar + 1)
				local currCoef = closeLocs[selectedAreaKey][3]
				local newSelectedLoc = (selectedArea[1] * currCoef + loc) / (currCoef + 1) 

				currCoef = currCoef + 1

				closeLocs[selectedAreaKey][1] = newSelectedLoc
				closeLocs[selectedAreaKey][2][currCoef] = key
				closeLocs[selectedAreaKey][3] = currCoef

				isCloseToAlreadySelected = true
			end
		end 

		-- location from a completely new area
		if isCloseToAlreadySelected == false then
			closeLocs[closeLocId] = {loc, {key}, 1}
			closeLocId = closeLocId + 1
		end
		
	end
	
	for i=1, #closeLocs do
		closeLocs[i] = closeLocs[i][1]
	end 

	return closeLocs
end