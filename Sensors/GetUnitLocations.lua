local sensorInfo = {
	name = "GetUnitLocations",
	desc = "Return location of all units keyed by their unitID.",
	author = "Federico Sergio",
	date = "2020-07-13",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition

-- @description returns map of locations of all units keyed by their unitID
return function(unitIDs)

	locations = {}
	for i=1,#unitIDs do 
		local unitID = unitIDs[i]
		local x,y,z = SpringGetUnitPosition(unitID)
		locations[unitID] = Vec3(x,y,z)
	end

	return locations
end