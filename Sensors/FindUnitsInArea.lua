local sensorInfo = {
	name = "FindUnitsInArea",
	desc = "Find unit ids that corresponds to units in certain area.",
	author = "Federico Sergio",
	date = "2020-07-22",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

local SpringGetUnitPosition = Spring.GetUnitPosition
function GetPosition(unitID) 
	local x,y,z = SpringGetUnitPosition(unitID)
	return Vec3(x,y,z)
end

-- @description filters out unit ids that corresponds to units in certain area
return function(unitIDs, areaCenter, areaRadius)

	local notFilteredUnitIDs = {}
	local j = 1

	for i=1, #unitIDs do

		local unitID = unitIDs[i]
		local unitLoc = GetPosition(unitID)

		if areaCenter:Distance(unitLoc) > areaRadius then
			notFilteredUnitIDs[j] = unitID
			j = j + 1
		end
	end

	return notFilteredUnitIDs

end