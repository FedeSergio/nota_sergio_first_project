local sensorInfo = {
	name = "MapUnitIDs",
	desc = "Give units apposite IDs.",
	author = "Federico Sergio",
	date = "2020-07-02",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- @description divides units into a number of groups
return function()

    local unitIDs = {}
    for x=1, units.length do
        unitIDs[x] = units[x]
    end

	return unitIDs

end